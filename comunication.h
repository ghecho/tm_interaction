// the user is responsible to include the correct header for uint8_t data types

#define PACKET_VERSION 1
#define TEAM_IDENTIFIER "Team-Mexico-2014"

enum TMPlayerRole
{
	TM_GOALIE_ROLE,
	TM_DEFENDER_ROLE,
	TM_STRIKER_ROLE,
	TM_INJURED_ROLE
};

enum TMFieldZone
{
	TM_FIELD_ZONE_UNKNOWN,
	TM_FIELD_ZONE_OUTSIDE,
	TM_FIELD_ZONE_NGA,
	TM_FIELD_ZONE_W4,
	TM_FIELD_ZONE_CW4,
	TM_FIELD_ZONE_CE4,
	TM_FIELD_ZONE_E4,
	TM_FIELD_ZONE_W3,
	TM_FIELD_ZONE_CW3,
	TM_FIELD_ZONE_CE3,
	TM_FIELD_ZONE_E3,
	TM_FIELD_ZONE_NC,
	TM_FIELD_ZONE_SC,
	TM_FIELD_ZONE_W2,
	TM_FIELD_ZONE_CW2,
	TM_FIELD_ZONE_CE2,
	TM_FIELD_ZONE_E2,
	TM_FIELD_ZONE_W1,
	TM_FIELD_ZONE_CW1,
	TM_FIELD_ZONE_CE1,
	TM_FIELD_ZONE_E1,
	TM_FIELD_ZONE_SGA
};

enum TMFieldOrientation
{
	TM_FIELD_ORIENTATION_NORTH,
	TM_FIELD_ORIENTATION_NORTH_EAST,
	TM_FIELD_ORIENTATION_EAST,
	TM_FIELD_ORIENTATION_SOUTH_EAST,
	TM_FIELD_ORIENTATION_SOUTH,
	TM_FIELD_ORIENTATION_SOUTH_WEST,
	TM_FIELD_ORIENTATION_WEST,
	TM_FIELD_ORIENTATION_NORTH_WEST
};

struct TMRobotPose
{
	enum TMFieldZone zone;
	uint8_t zone_certainty; //values (1-100) if zero return TM_FIELD_ZONE_UNKNOWN
	enum TMFieldOrientation orientation;
	uint8_t orientation_certainty; // values (0-100)
};

struct TMBallLocation
{
	enum TMFieldZone zone;
	uint8_t zone_certainty; //values (1-100) if zero return TM_FIELD_ZONE_UNKNOWN
};

struct TMBallETA
{
	uint8_t time_to_ball; //in seconds
	uint8_t time_certainty; //values (1-100) how certain I am that the time I am reporting is accurate
};

#define TM_CLEAR_FOR_PASS 1
#define TM_PREPARING_KICK (1<<1)
#define TM_WALKING_TOWARDS_BALL (1<<2)
#define TM_SEE_BALL (1<<3)
#define TM_FALLEN (1<<4)

struct packet
{
	uint8_t packet_version;//must be set to PACKET_VERSION
	char team_identifier[20];//must be set to TEAM_IDENTIFIER
	uint8_t robot_id;
	enum TMPlayerRole current_role;
	enum TMPlayerRole default_role;
	struct TMRobotPose own_location;
	struct TMBallLocation ball_location;
	struct TMBallETA ball_eta;
	uint8_t remaining_battery_time; //in minutes or zero if unknown
	uint8_t pass_sent_to; //robot_id of the robot to whom I sent a pass to or zero if no pass was sent
};









